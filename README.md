# sysswo
## 🚀 Goal
The Software Ontology (SwO) captures that software products have a complex artifactual nature, being constituted of software artifacts of different nature, including software systems, programs and code.

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest
3. Spring GraphQL


## 🔧 Install

1) Create a database with name sysswo with **CREATE DATABASE sysswo**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:](http://localhost:8081) to see Swagger
* Acess [http://localhost:/grapiql](http://localhost:8081/grapiql) to see Graphql.

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

