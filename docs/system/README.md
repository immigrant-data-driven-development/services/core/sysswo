# 📕Documentation: system

Module with some concepts of SysSWO.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **CommmonEntity** : -
* **Code** :  Code is a Software Item representing a set of computer instructions and data definitions expressed in a programming language or in a form output by an assembler, compiler, or another translator
* **SoftwareSystem** : Software Item that aims at satisfying a specification (System Specification), concerning a desired change in a data structure inside a computer, abstracting away from the behavior.
* **LoadedSoftwareSystemCopy** : A Loaded Software System Copy is a Disposition that is a materialization of a Software System, inhering in a Machine.
