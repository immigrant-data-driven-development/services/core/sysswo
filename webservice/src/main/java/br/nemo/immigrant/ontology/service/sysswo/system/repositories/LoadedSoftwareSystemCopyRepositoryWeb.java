package br.nemo.immigrant.ontology.service.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.LoadedSoftwareSystemCopy;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.LoadedSoftwareSystemCopyRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "loadedsoftwaresystemcopy", path = "loadedsoftwaresystemcopy")
public interface LoadedSoftwareSystemCopyRepositoryWeb extends LoadedSoftwareSystemCopyRepository {

}
