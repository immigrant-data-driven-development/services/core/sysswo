package br.nemo.immigrant.ontology.service.sysswo.system.records;
import java.time.LocalDate;
public record LoadedSoftwareSystemCopyInput( String name,String description,String externalId,String internalId ) {
}
