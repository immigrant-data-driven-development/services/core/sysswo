package br.nemo.immigrant.ontology.service.sysswo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.nemo.immigrant.ontology.service.sysswo.*"})
@EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
@EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.service.sysswo.*"})
@OpenAPIDefinition(info = @Info(
  title = "System and Software Ontology WebService",
  version = "1.0",
  description = "The Software Ontology (SwO) captures that software products have a complex artifactual nature, being constituted of software artifacts of different nature, including software systems, programs and code."))

public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
