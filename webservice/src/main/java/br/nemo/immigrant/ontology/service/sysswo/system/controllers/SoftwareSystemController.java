package br.nemo.immigrant.ontology.service.sysswo.system.controllers;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.SoftwareSystem;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.SoftwareSystemRepository;
import br.nemo.immigrant.ontology.service.sysswo.system.records.SoftwareSystemInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SoftwareSystemController  {

  @Autowired
  SoftwareSystemRepository repository;

  @QueryMapping
  public List<SoftwareSystem> findAllSoftwareSystems() {
    return repository.findAll();
  }

  @QueryMapping
  public SoftwareSystem findByIDSoftwareSystem(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public SoftwareSystem createSoftwareSystem(@Argument SoftwareSystemInput input) {
    SoftwareSystem instance = SoftwareSystem.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public SoftwareSystem updateSoftwareSystem(@Argument Long id, @Argument SoftwareSystemInput input) {
    SoftwareSystem instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("SoftwareSystem not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteSoftwareSystem(@Argument Long id) {
    repository.deleteById(id);
  }

}
