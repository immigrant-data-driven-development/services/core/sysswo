package br.nemo.immigrant.ontology.service.sysswo.system.controllers;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.LoadedSoftwareSystemCopy;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.LoadedSoftwareSystemCopyRepository;
import br.nemo.immigrant.ontology.service.sysswo.system.records.LoadedSoftwareSystemCopyInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class LoadedSoftwareSystemCopyController  {

  @Autowired
  LoadedSoftwareSystemCopyRepository repository;

  @QueryMapping
  public List<LoadedSoftwareSystemCopy> findAllLoadedSoftwareSystemCopys() {
    return repository.findAll();
  }

  @QueryMapping
  public LoadedSoftwareSystemCopy findByIDLoadedSoftwareSystemCopy(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public LoadedSoftwareSystemCopy createLoadedSoftwareSystemCopy(@Argument LoadedSoftwareSystemCopyInput input) {
    LoadedSoftwareSystemCopy instance = LoadedSoftwareSystemCopy.builder().name(input.name()).
                                                                           description(input.description()).
                                                                           externalId(input.externalId()).
                                                                           internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public LoadedSoftwareSystemCopy updateLoadedSoftwareSystemCopy(@Argument Long id, @Argument LoadedSoftwareSystemCopyInput input) {
    LoadedSoftwareSystemCopy instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("LoadedSoftwareSystemCopy not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteLoadedSoftwareSystemCopy(@Argument Long id) {
    repository.deleteById(id);
  }

}
