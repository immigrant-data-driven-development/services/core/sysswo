package br.nemo.immigrant.ontology.service.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.Code;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.CodeRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "code", path = "code")
public interface CodeRepositoryWeb extends CodeRepository {

}
