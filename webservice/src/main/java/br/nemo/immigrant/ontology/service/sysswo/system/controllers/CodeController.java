package br.nemo.immigrant.ontology.service.sysswo.system.controllers;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.Code;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.CodeRepository;
import br.nemo.immigrant.ontology.service.sysswo.system.records.CodeInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CodeController  {

  @Autowired
  CodeRepository repository;

  @QueryMapping
  public List<Code> findAllCodes() {
    return repository.findAll();
  }

  @QueryMapping
  public Code findByIDCode(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Code createCode(@Argument CodeInput input) {
    Code instance = Code.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Code updateCode(@Argument Long id, @Argument CodeInput input) {
    Code instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Code not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCode(@Argument Long id) {
    repository.deleteById(id);
  }

}
