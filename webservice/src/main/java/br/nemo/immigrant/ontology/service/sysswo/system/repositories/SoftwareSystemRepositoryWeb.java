package br.nemo.immigrant.ontology.service.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.SoftwareSystem;
import br.nemo.immigrant.ontology.entity.sysswo.system.repositories.SoftwareSystemRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "softwaresystem", path = "softwaresystem")
public interface SoftwareSystemRepositoryWeb extends SoftwareSystemRepository {

}
