package br.nemo.immigrant.ontology.entity.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.Code;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CodeRepository extends PagingAndSortingRepository<Code, Long>, ListCrudRepository<Code, Long> {

}
