package br.nemo.immigrant.ontology.entity.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.SoftwareSystem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface SoftwareSystemRepository extends PagingAndSortingRepository<SoftwareSystem, Long>, ListCrudRepository<SoftwareSystem, Long> {

}
