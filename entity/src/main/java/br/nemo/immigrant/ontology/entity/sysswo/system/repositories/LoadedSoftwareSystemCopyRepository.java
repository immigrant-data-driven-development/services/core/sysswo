package br.nemo.immigrant.ontology.entity.sysswo.system.repositories;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.LoadedSoftwareSystemCopy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface LoadedSoftwareSystemCopyRepository extends PagingAndSortingRepository<LoadedSoftwareSystemCopy, Long>, ListCrudRepository<LoadedSoftwareSystemCopy, Long> {

}
